import { showModal } from './modal';

export function showWinnerModal(fighter) {
  const modalOutput = {
    title: `${fighter.name} WINS!`,
    bodyElement: `Tap 'close' button to reload the page`,
    onClose: () => document.location.reload()
  }
  showModal(modalOutput);
}
