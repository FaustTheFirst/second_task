import { keyDownHandler, keyUpHandler } from "../helpers/keyHandlers";

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const fightProperties = {
      fighters: [{...firstFighter}, {...secondFighter}],
      playersHealth: [100, 100],
      playersHealthBars: [
        document.getElementById('left-fighter-indicator'),
        document.getElementById('right-fighter-indicator')
      ],
      controlsInput: new Set()
    };

    const keyDownListener = e => {
      keyDownHandler(fightProperties, e.code, e.repeat);
      const checkHealth = fightProperties.playersHealth.some(elem => (elem <= 0));
      if (checkHealth) {
        document.removeEventListener('keydown', keyDownListener);
        document.removeEventListener('keyup', keyUpListener);
        fightProperties.playersHealth.reverse();
        const winnerIndex = fightProperties.playersHealth.findIndex(elem => (elem <= 0));
        resolve(fightProperties.fighters[winnerIndex]);
      }
    }

    const keyUpListener = e => keyUpHandler(fightProperties, e.code);

    document.addEventListener('keydown', keyDownListener);
    document.addEventListener('keyup', keyUpListener);
  });
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker.attack) - getBlockPower(defender.defense);
  return Math.max(damage, 0);
}

export function getHitPower(attack) {
  const criticalHitChance = Math.random() + 1;
  return attack * criticalHitChance;
}

export function getBlockPower(defense) {
  const dodjeChance = Math.random() + 1;
  return defense * dodjeChance;
}
