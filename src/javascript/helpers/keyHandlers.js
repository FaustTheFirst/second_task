import { controls } from '../../constants/controls';
import { applyAttack } from './fightHelper';

export const keyDownHandler = (fightProperties, keyCode, isKeyRepeat) => {
  if(!isKeyRepeat) {
    switch(keyCode) {
      case controls.PlayerOneAttack: {
        fightProperties.controlsInput.add(controls.PlayerOneAttack);
        applyAttack(fightProperties, 0, 1);
        break;
      }

      case controls.PlayerTwoAttack: {
        fightProperties.controlsInput.add(controls.PlayerTwoAttack);
        applyAttack(fightProperties, 1, 0);
        break;
      }

      case controls.PlayerOneBlock: {
        fightProperties.controlsInput.add(controls.PlayerOneBlock);
        break;
      }

      case controls.PlayerTwoBlock: {
        fightProperties.controlsInput.add(controls.PlayerTwoBlock);
        break;
      }
    }
  }
}

export const keyUpHandler = (fightProperties, keyCode) => {
  if (fightProperties.controlsInput.has(keyCode)) {
    fightProperties.controlsInput.delete(keyCode);
  }
}
