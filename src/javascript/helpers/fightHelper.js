import { controls } from '../../constants/controls';
import { getDamage } from '../components/fight';
import { showStatus } from './domHelper';

const checkIfAttackWhileBlock = controlsInput => (controlsInput.has(controls.PlayerOneAttack)
  && controlsInput.has(controls.PlayerOneBlock))
  || (controlsInput.has(controls.PlayerTwoAttack)
  && controlsInput.has(controls.PlayerTwoBlock));

const checkIfBlock = controlsInput => controlsInput.has(controls.PlayerOneBlock)
  || controlsInput.has(controls.PlayerTwoBlock);

export const applyAttack = (fightProperties, attacker, defender) => {
  if (checkIfAttackWhileBlock(fightProperties.controlsInput)) {
      showStatus('Can`t attack', attacker);
      return;
  }

  if (checkIfBlock(fightProperties.controlsInput)) {
      showStatus('block', defender);
      return;
  }

  const damageDealt = getDamage(fightProperties.fighters[attacker], fightProperties.fighters[defender]);

  if (!damageDealt) {
    showStatus('miss', attacker);
    return;
  }

  showStatus(`-${damageDealt.toFixed(1)}`, defender);

  fightProperties.playersHealth[defender] = fightProperties.playersHealth[defender] - damageDealt / fightProperties.fighters[defender].health * 100;
  fightProperties.playersHealthBars[defender].style.width = `${fightProperties.playersHealth[defender]}%`;
}
