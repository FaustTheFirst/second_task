export function createElement({ tagName, className, attributes = {} }) {
  const element = document.createElement(tagName);

  if (className) {
    const classNames = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  Object.keys(attributes).forEach((key) => element.setAttribute(key, attributes[key]));

  return element;
}

export const showStatus = (text, position) => {
  const activeStatusMarker = document.getElementById(position);

  if (activeStatusMarker) {
    activeStatusMarker.remove();
  }

  const statusMarker = createElement({ tagName: 'div', className: 'arena___status-marker', attributes: { id: position } });  

  statusMarker.innerText = text;

  document.getElementsByClassName('arena___health-indicator')[position]
    .appendChild(statusMarker).focus();
  document.getElementById(position)
  .classList.replace('arena___status-marker', 'arena___status-marker-after');
}
